﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace WindowsFormsApplication2
{
    public partial class Form2 : Form
    {
        Form opener;
        private SqlConnection connection;
        private SqlCommand command;
        private SqlDataAdapter adapter;
        private DataTable table;
        public string Name;
        public string Surname;
        public int Ident_Earnings;
        public int Ident_Department;
        public int Ident_Departmentsc;
        public int Ident_Departmentth;
        public int Ident_Departmentfrth;
        public int click;
        public int Ident_Personality;
        Form1 form1;
        public Form2(Form parentForm)
        {
            InitializeComponent();
            opener = parentForm;
            form1 = new Form1();
        }
     
         


        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                if (textBox4.Text != textBox5.Text)
                {
                    if (textBox4.Text != textBox6.Text)
                    {


                        if (textBox1.Text.Length > 1)
                        {
                            if (textBox2.Text.Length > 1)
                            {
                                if (Convert.ToInt16(textBox3.Text) > 18 && Convert.ToInt16(textBox3.Text) < 65)
                                {
                                    using (SqlConnection connection = new SqlConnection("Server=localhost;Database=Company;Trusted_Connection=True;Connect Timeout=10;"))
                                    {
                                        Name = textBox1.Text.ToString();
                                        Surname = textBox2.Text.ToString();
                                        textBox2.Text = textBox2.Text.Substring(0, 1).ToUpper() + textBox2.Text.Substring(1);

                                        SqlCommand cmd = new SqlCommand(String.Format("Insert into Personality(Name,Surname,Age)values(@Name,@Surname,@Age);Select @@IDENTITY AS 'Ident_Personality';"));
                                        cmd.CommandType = CommandType.Text;
                                        cmd.Connection = connection;
                                        cmd.Parameters.AddWithValue("@Name", textBox1.Text);
                                        cmd.Parameters.AddWithValue("@Surname", textBox2.Text);
                                        cmd.Parameters.AddWithValue("@Age", Convert.ToInt16(textBox3.Text));
                                        connection.Open();
                                        SqlDataReader dr = cmd.ExecuteReader();

                                        while (dr.Read())
                                        {
                                            Ident_Personality = Convert.ToInt32(dr["Ident_Personality"]);
                                        }
                                        dr.Close();
                                    }
                                    using (SqlConnection connection = new SqlConnection("Server=localhost;Database=Company;Trusted_Connection=True;Connect Timeout=10;"))
                                    {
                                        SqlCommand cmd = new SqlCommand(String.Format("Insert into Earnings(Earnings_Monthly,Earnings_Annual)values(@Earnings_Monthly,@Earnings_Annual);Select @@IDENTITY AS 'Ident_Earnings';"));
                                        cmd.CommandType = CommandType.Text;
                                        cmd.Connection = connection;
                                        cmd.Parameters.AddWithValue("@Earnings_Monthly", 1);
                                        cmd.Parameters.AddWithValue("@Earnings_Annual", 1);
                                        connection.Open();
                                        SqlDataReader dr = cmd.ExecuteReader();

                                        while (dr.Read())
                                        {
                                            Ident_Earnings = Convert.ToInt32(dr["Ident_Earnings"]);
                                        }
                                        dr.Close();
                                    }
                                    using (SqlConnection connection = new SqlConnection("Server=localhost;Database=Company;Trusted_Connection=True;Connect Timeout=10;"))
                                    {
                                        SqlCommand cmd = new SqlCommand(String.Format("Insert into Ident(Ident_Personality,Ident_Earnings,Ident_Department)values(@Ident_Personality,@Ident_Earnings,@Ident_Department);Insert into Ident(Ident_Personality,Ident_Earnings,Ident_Department)values(@Ident_Personality,@Ident_Earnings,@Ident_Departmentsc);Insert into Ident(Ident_Personality,Ident_Earnings,Ident_Department)values(@Ident_Personality,@Ident_Earnings,@Ident_Departmentth);Delete from Ident where Ident_Department=0;"));
                                        cmd.CommandType = CommandType.Text;
                                        cmd.Connection = connection;
                                        cmd.Parameters.AddWithValue("@Ident_Personality", Ident_Personality);
                                        cmd.Parameters.AddWithValue("@Ident_Earnings", Ident_Earnings);
                                        cmd.Parameters.AddWithValue("@Ident_Department", Ident_Department);
                                        cmd.Parameters.AddWithValue("@Ident_Departmentsc", Ident_Departmentsc);
                                        cmd.Parameters.AddWithValue("@Ident_Departmentth", Ident_Departmentth);
                                        connection.Open();
                                        cmd.ExecuteNonQuery();
                                    }
                                    this.Close();
                                    form1.ShowDialog(); 
                                }
                                else
                                {
                                    MessageBox.Show("Minimum Age is 18, maximum 65");
                                }
                            }
                            else
                            {
                                MessageBox.Show("Minimum surname length is 2 letters");
                            }

                        }
                        else
                        {
                            MessageBox.Show("Minimum name length is 2 letters");
                        }
                    }
                    else
                    {
                        MessageBox.Show("One person can be added to one department only once");
                    }
                }
                else
                {
                    MessageBox.Show("One person can be added to one department only once");
                }
               
            }
            catch(SqlException)
            {
                MessageBox.Show("Problem with connection to database");
            }
            
        }

        private void textBox1_Leave(object sender, EventArgs e)
        {
            if (textBox1.Text.Length > 2)
            {
                textBox1.Text = textBox1.Text.Substring(0, 1).ToUpper() + textBox1.Text.Substring(1);
            }
        }

        private void textBox2_Leave(object sender, EventArgs e)
        {
            if (textBox2.Text.Length > 2)
            {
                textBox2.Text = textBox2.Text.Substring(0, 1).ToUpper() + textBox2.Text.Substring(1);
            }
        }

        private void textBox3_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsDigit(e.KeyChar) && e.KeyChar != (char)13 && e.KeyChar != (char)8 && e.KeyChar != (char)127)
            {
                e.Handled = true;
            }
        }

        private void Form2_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'companyDataSet.Personality' table. You can move, or remove it, as needed.
            this.personalityTableAdapter.Fill(this.companyDataSet.Personality);
            // TODO: This line of code loads data into the 'companyDataSet.Departments' table. You can move, or remove it, as needed.
            this.departmentsTableAdapter.Fill(this.companyDataSet.Departments);
            dataGridView1.Hide();
            textBox5.Hide();
            textBox6.Hide();
            button4.Hide();
            button3.Hide();
        }

        
        private void button2_Click(object sender, EventArgs e)
        {
            ++click;
            if (click == 1)
            {
                button3.Show();
                textBox5.Show();
                dataGridView1.Show();
                dataGridView1.DataSource = companyDataSet.Departments;
            }
            if(click==2)
            {
                if (textBox5.Text.Length > 2)
                {
                    button4.Show();
                    textBox6.Show();
                }
                else
                {
                    click = 1;
                    MessageBox.Show("Select second department first");
                }
            }
            if(click==3)
            {
                if (textBox6.Text.Length > 2)
                {
                    if (textBox5.Text.Length > 2)
                    {
                        MessageBox.Show("One person can be in maximum 3 departments");
                    }
                }
                else
                {
                    if (textBox5.Text.Length < 2)
                    {
                        MessageBox.Show("Select second department first");
                    }
                    else
                    {
                        MessageBox.Show("Select third department first");
                    }
                }
            }
        }

        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (click == 1)
            {
                Ident_Departmentsc = Convert.ToInt16(dataGridView1.Rows[e.RowIndex].Cells[1].Value.ToString());
                textBox5.Text = dataGridView1.Rows[e.RowIndex].Cells[0].Value.ToString();
            }
            if(click==2)
            {
                  Ident_Departmentth = Convert.ToInt16(dataGridView1.Rows[e.RowIndex].Cells[1].Value.ToString());
                textBox6.Text = dataGridView1.Rows[e.RowIndex].Cells[0].Value.ToString();
            }

        }

        private void button3_Click(object sender, EventArgs e)
        {

            textBox5.Clear();
           
        }

        private void button4_Click(object sender, EventArgs e)
        {
            textBox6.Clear();
        }


        private void textBox5_Enter(object sender, EventArgs e)
        {
            click = 1;
        }

        private void textBox6_Enter(object sender, EventArgs e)
        {
            click = 2;
        }

        private void textBox7_Enter(object sender, EventArgs e)
        {
            click = 3;
        }

        private void textBox5_TextChanged(object sender, EventArgs e)
        {
            if(textBox4.Text==textBox5.Text)
            {
                MessageBox.Show("One person can be added to one department only once");
                textBox5.Clear();
            }
            if (textBox6.Text.Length > 1)
            {
                if (textBox5.Text == textBox6.Text)
                {
                    MessageBox.Show("One person can be added to one department only once");
                    textBox5.Clear();
                }
            }
        }

        private void textBox6_TextChanged(object sender, EventArgs e)
        {
            if (textBox4.Text == textBox6.Text)
            {
                MessageBox.Show("One person can be added to one department only once");
                textBox6.Clear();
            }
            if (textBox5.Text.Length > 1)
            {
                if (textBox5.Text == textBox6.Text)
                {
                    MessageBox.Show("One person can be added to one department only once");
                    textBox6.Clear();
                }
            }

        }


    }
}
