﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;



namespace WindowsFormsApplication2
{
    public partial class Form1 : Form
    {
        private SqlConnection connection;
        private SqlCommand command;
        private SqlDataAdapter adapter;
        private DataTable table;
        public string Department;
        string Name;
        string Surname;
        int Age;
        string Ident;
        public int Ident_Personality;
        public int Ident_Department;
        string Personality;
        string Earnings;
        public int Ident_Earnings;
        Form2 form2;
        Form3 form3;
        
       
        public Form1()
        {
            InitializeComponent();
            textBox1.Hide();
            textBox2.Hide();
            textBox3.Hide();
            
        }


        private void Form1_Load(object sender, EventArgs e)
        {
            try
            { 
            dataGridView1.DataSource = companyDataSet.Departments;
            this.departmentsTableAdapter.Fill(this.companyDataSet.Departments);
            dataGridView1.ReadOnly = true;
            dataGridView2.ReadOnly = true;
            dataGridView2.DataSource = companyDataSet.Personality;
            this.personalityTableAdapter1.Fill(this.companyDataSet.Personality);
            }
            catch (SqlException)
            {
                MessageBox.Show("Something is wrong with connection");
            }
            
            
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if(Ident_Department>0)
            {
                
                form2 = new Form2(this);
                form2.Show();
                form2.textBox4.Text = textBox8.Text;
                form2.Ident_Department = Ident_Department;
            }
            else
            {
                MessageBox.Show("Select Department first");
            }
            
            
        }
        private void dataGridView1_CellClick(object sender, System.Windows.Forms.DataGridViewCellEventArgs e)
        {
            int wiersze=dataGridView2.Rows.Count;
            if (dataGridView1.Rows[e.RowIndex].Cells[0].Value.ToString() != null)
            {
                Department = dataGridView1.Rows[e.RowIndex].Cells[0].Value.ToString();
                textBox8.Text = Department;
            }
            try
            { 
                 using (SqlConnection connection = new SqlConnection("Server=localhost;Database=Company;Trusted_Connection=True;Connect Timeout=10;"))
                 {
                     DataTable dataTable = new DataTable();

                     SqlCommand cmd = new SqlCommand(String.Format("Select * From Personality Where Department=@Department; "));
                     cmd.CommandType = CommandType.Text;
                     cmd.Connection = connection;
                     cmd.Parameters.AddWithValue("Department", Department);
                     connection.Open();
                     SqlDataAdapter da = new SqlDataAdapter(cmd);
                     da.Fill(dataTable);
                     connection.Close();
                     da.Dispose();
                     dataGridView2.DataSource = dataTable;
                 }
            }
            catch (SqlException)
            {
                MessageBox.Show("Something is wrong with connection");
            }
        }
            

        private void button2_Click(object sender, EventArgs e)
        {
            if ( Ident_Personality>0&& Ident_Earnings>0)
            {
                try
                {
                SqlConnection connection = new SqlConnection("Server=localhost;Database=Company;Trusted_Connection=True;Connect Timeout=10;");
                string sqlStatement = String.Format("Delete From Personality where Ident_Personality=@Ident_Personality;Delete from Earnings where Ident_Earnings=@Ident_Earnings;Delete From Ident Where Ident_Personality=@Ident_Personality AND Ident_Earnings=@Ident_Earnings AND Ident_Department=@Ident_Department;");
               
                    connection.Open();
                    SqlCommand cmd = new SqlCommand(sqlStatement, connection);
                    cmd.Parameters.AddWithValue("Ident_Personality",Ident_Personality);
                    cmd.Parameters.AddWithValue("Ident_Earnings", Ident_Earnings);
                    cmd.Parameters.AddWithValue("Ident_Department", Ident_Department);
                    cmd.CommandType = CommandType.Text;
                    cmd.ExecuteNonQuery();
                }
                catch (SqlException sqlEx)
                {
                    MessageBox.Show(sqlEx.Message);
                }
            }
            else
            {
                MessageBox.Show("Select data to delete");
            }
           
            textBox6.Clear();
            textBox7.Clear();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            if (Ident_Personality > 0)
            {
                form3 = new Form3();
                form3.Show();
                form3.textBox1.Text = Name;
                form3.textBox2.Text = Surname;
                form3.textBox3.Text = Age.ToString();
                form3.textBox4.Text = textBox6.Text;
                form3.textBox5.Text = textBox7.Text;
                form3.Ident_Earnings = Convert.ToInt16(textBox5.Text);
                form3.Ident_Personality = Convert.ToInt16(Ident_Personality);
            }
            else
            {
                MessageBox.Show("Select person to update");
            }
        }

        private void dataGridView1_CellLeave(object sender, System.Windows.Forms.DataGridViewCellEventArgs e)
        {

            //button3_Click(this, new EventArgs());
        }



        private void button4_Click(object sender, EventArgs e)
        {
            textBox1.Clear();
            textBox2.Clear();
            textBox3.Clear();
            textBox6.Clear();
            textBox5.Clear();
            textBox7.Clear();
            Ident="";
        }

        private void textBox6_KeyPress(object sender, System.Windows.Forms.KeyPressEventArgs e)
        {
            if (!char.IsDigit(e.KeyChar) && !char.IsControl(e.KeyChar))
            {
                e.Handled = true;
                MessageBox.Show("Enter earnings monthly in this textbox");
            }

        }

        private void textBox3_KeyPress(object sender, System.Windows.Forms.KeyPressEventArgs e)
        {
            if (!char.IsDigit(e.KeyChar) && !char.IsControl(e.KeyChar))
            {
                e.Handled = true;
                MessageBox.Show("Enter age in this textbox");
            }
        }

        private void textBox7_KeyPress(object sender, System.Windows.Forms.KeyPressEventArgs e)
        {
            if (!char.IsDigit(e.KeyChar)&&!char.IsControl(e.KeyChar))
            {
                e.Handled = true;
                MessageBox.Show("Enter earnings annualy in this textbox");
            }
        }

        private void button5_Click(object sender, EventArgs e)
        {
            if (textBox4.Text.Length >=4)
            {
                textBox4.Text = textBox4.Text.Substring(0, 1).ToUpper() + textBox4.Text.Substring(1);
                try
                {
                    string department = textBox4.Text;
                    using (SqlConnection connection = new SqlConnection("Server=localhost;Database=Company;Trusted_Connection=True;Connect Timeout=10;"))
                    {
                        SqlCommand cmd = new SqlCommand(String.Format("INSERT INTO Departments (Department) VALUES (@Department);"));
                        cmd.CommandType = CommandType.Text;
                        cmd.Connection = connection;
                        cmd.ToString();
                        cmd.Parameters.AddWithValue("@Department", textBox4.Text.ToString());
                        connection.Open();
                        cmd.ExecuteNonQuery();
                        connection.Close();
                    }
                    dataGridView1.DataSource = companyDataSet.Departments;
                    this.departmentsTableAdapter.Fill(this.companyDataSet.Departments);
                    textBox4.Clear();
                }
                catch
                {
                    MessageBox.Show("Problem with connection");
                }
               
            }
            else
            {
                MessageBox.Show("Enter department name, minimum 4 letters");
            }

        }

        private void dataGridView2_CellClick(object sender, System.Windows.Forms.DataGridViewCellEventArgs e)
        {
            try
            {
                if (dataGridView2.Rows[e.RowIndex].Cells[3].Value.ToString() != null && dataGridView2.Rows[e.RowIndex].Cells[1].Value.ToString().Length > 2)
                {
                    Ident_Personality = Convert.ToInt16(dataGridView2.Rows[e.RowIndex].Cells[3].Value.ToString());
                    Name = dataGridView2.Rows[e.RowIndex].Cells[0].Value.ToString();
                    Surname = dataGridView2.Rows[e.RowIndex].Cells[1].Value.ToString();
                    Age = Convert.ToInt16(dataGridView2.Rows[e.RowIndex].Cells[2].Value.ToString());
                    SqlConnection con = new SqlConnection("Server=localhost;Database=Company;Trusted_Connection=True;");
                    string sql = string.Format("Select * From Earnings Inner Join Ident On Earnings.Ident_Earnings=Ident.Ident_Earnings Where Ident.Ident_Personality=@Ident");
                    int Monthly;
                    int Annual;
                    con.Open();
                    SqlCommand cmd = new SqlCommand(sql, con);
                    cmd.Parameters.AddWithValue("Ident", Ident_Personality);
                    SqlDataReader dr = cmd.ExecuteReader();
                    while (dr.Read())
                    {

                        Monthly = Convert.ToInt32(dr["Earnings_Monthly"]);
                        Annual = Convert.ToInt32(dr["Earnings_Annual"]);
                        Ident_Earnings = Convert.ToInt32(dr["Ident_Earnings"]);
                        textBox5.Text = Ident_Earnings.ToString();
                        textBox6.Text = Monthly.ToString();
                        textBox7.Text = Annual.ToString();
                    }
                    using (SqlConnection connection = new SqlConnection("Server=localhost;Database=Company;Trusted_Connection=True;Connect Timeout=10;"))
                    {
                        DataTable dataTable = new DataTable();

                        SqlCommand comenda = new SqlCommand(String.Format("Select Department,Departments.Ident_Department from Departments inner Join Ident on Departments.Ident_Department=Ident.Ident_Department Where Ident.Ident_Personality=@Ident_Personality;"));
                        comenda.CommandType = CommandType.Text;
                        comenda.Connection = connection;
                        comenda.Parameters.AddWithValue("@Ident_Personality", Ident_Personality);
                        connection.Open();
                        SqlDataAdapter da = new SqlDataAdapter(comenda);
                        da.Fill(dataTable);
                        connection.Close();
                        da.Dispose();
                        dataGridView3.DataSource = dataTable;
                    }
                }
            }
            catch (SqlException)
            {
                MessageBox.Show("Problem with connection to database");
            }
        }

        private void textBox1_Leave(object sender, EventArgs e)
        {
            if (textBox1.Text.Length > 1)
            {
                textBox1.Text = textBox1.Text.Substring(0, 1).ToUpper() + textBox1.Text.Substring(1);
            }
            
        }

        private void textBox2_Leave(object sender, EventArgs e)
        {
            if(textBox2.Text.Length>1)
            {
                textBox2.Text = textBox2.Text.Substring(0, 1).ToUpper() + textBox2.Text.Substring(1);
            }
        }


        private void dataGridView1_CellClick_1(object sender, System.Windows.Forms.DataGridViewCellEventArgs e)
        {
            if (Convert.ToInt16(dataGridView1.Rows[e.RowIndex].Cells[1].Value.ToString())>0)
            {
                Department = dataGridView1.Rows[e.RowIndex].Cells[0].Value.ToString();
                textBox8.Text = Department;
                Ident_Department = Convert.ToInt16(dataGridView1.Rows[e.RowIndex].Cells[1].Value.ToString());
                try
                {
                    using (SqlConnection connection = new SqlConnection("Server=localhost;Database=Company;Trusted_Connection=True;Connect Timeout=10;"))
                    {
                        DataTable dataTable = new DataTable();

                        SqlCommand cmd = new SqlCommand(String.Format("Select * From Personality Inner Join Ident On Personality.Ident_Personality=Ident.Ident_Personality Where Ident.Ident_Department=@Department "));
                        cmd.CommandType = CommandType.Text;
                        cmd.Connection = connection;
                        cmd.Parameters.AddWithValue("@Department", Ident_Department);
                        connection.Open();
                        SqlDataAdapter da = new SqlDataAdapter(cmd);
                        da.Fill(dataTable);
                        connection.Close();
                        da.Dispose();
                        dataGridView2.DataSource = dataTable;
                    }
                }
                catch(SqlException)
                {
                    MessageBox.Show("Problem with connection to database");
                }

            }
        }

        private void button6_Click(object sender, EventArgs e)
        {
            textBox1.Clear();
            textBox2.Clear();
            textBox3.Clear();
            textBox6.Clear();
            textBox5.Clear();
            textBox7.Clear();
            textBox8.Clear();
        }

        private void button4_Click_2(object sender, EventArgs e)
        {
            Form frm = new Form();
            frm.ShowDialog();
        }

        private void button4_Click_1(object sender, EventArgs e)
        {
            using (SqlConnection connection = new SqlConnection("Server=localhost;Database=Company;Trusted_Connection=True;Connect Timeout=10;"))
            {
                DataTable dataTable = new DataTable();

                SqlCommand cmd = new SqlCommand(String.Format("Select * From Personality Inner Join Ident On Personality.Ident_Personality=Ident.Ident_Personality Where Ident.Ident_Department=@Department "));
                cmd.CommandType = CommandType.Text;
                cmd.Connection = connection;
                cmd.Parameters.AddWithValue("@Department", Ident_Department);
                connection.Open();
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(dataTable);
                connection.Close();
                da.Dispose();
                dataGridView2.DataSource = dataTable;
            }
        }

        private void button7_Click(object sender, EventArgs e)
        {
            if (Ident_Department > 0)
            {
                try
                {
                SqlConnection connection = new SqlConnection("Server=localhost;Database=Company;Trusted_Connection=True;Connect Timeout=10;");
                string sqlStatement = String.Format("Delete From Departments where Ident_Department=@Ident_Department;Delete from Ident where Ident_Department=@Ident_Department;");
                    connection.Open();
                    SqlCommand cmd = new SqlCommand(sqlStatement, connection);
                    cmd.Parameters.AddWithValue("Ident_Department", Ident_Department);
                    cmd.CommandType = CommandType.Text;
                    cmd.ExecuteNonQuery();
                    dataGridView1.DataSource = companyDataSet.Departments;
                    this.departmentsTableAdapter.Fill(this.companyDataSet.Departments);
                }
                catch (SqlException sqlEx)
                {
                    MessageBox.Show(sqlEx.Message);
                }
            }
            else
            {
                MessageBox.Show("Select department to delete");
            }
           
        }

        private void textBox4_Leave(object sender, EventArgs e)
        {
            if(textBox4.Text.Length>1)
            {
                textBox4.Text = textBox4.Text.Substring(0, 1).ToUpper() + textBox4.Text.Substring(1);
            }
        }

    }
}
