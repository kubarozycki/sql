﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace WindowsFormsApplication2
{
    public partial class Form3 : Form
    {
        public int Ident_Earnings;
        public int Ident_Personality;
        public int Ident_Department;
        Form opener;
        Form1 form1;
        public Form3()
        {
            InitializeComponent();
            form1 = new Form1();
        }

        private void Form3_Load(object sender, EventArgs e)
        {
            dataGridView2.Hide();
            dataGridView1.Hide();
            label6.Hide();
            label7.Hide();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                if (textBox1.Text.Length > 1)
                {
                    if (textBox2.Text.Length > 1)
                    {
                        if (Convert.ToInt16(textBox3.Text) > 18 && Convert.ToInt16(textBox3.Text) < 65)
                        {
                            if (Convert.ToInt16(textBox4.Text) > 0)
                            {
                                if (Convert.ToInt16(textBox5.Text) > 0)
                                {
                                    using (SqlConnection connection = new SqlConnection("Server=localhost;Database=Company;Trusted_Connection=True;"))
                                    {
                                        SqlCommand cmd = new SqlCommand(String.Format("Update Personality Set Name=@Name,Surname=@Surname,Age=@Age Where Ident_Personality=@Ident_Personality;Update Earnings Set Earnings_Monthly=@Earnings_Monthly,Earnings_Annual=@Earnings_Annual Where Ident_Earnings=@Ident_Earnings;"));
                                        cmd.CommandType = CommandType.Text;
                                        cmd.Connection = connection;
                                        cmd.Parameters.AddWithValue("@Name", textBox1.Text);
                                        cmd.Parameters.AddWithValue("@Surname", textBox2.Text);
                                        cmd.Parameters.AddWithValue("@Age", Convert.ToInt16(textBox3.Text));
                                        cmd.Parameters.AddWithValue("@Ident_Personality", Ident_Personality);
                                        cmd.Parameters.AddWithValue("@Earnings_Monthly", textBox4.Text);
                                        cmd.Parameters.AddWithValue("@Earnings_Annual", textBox5.Text);
                                        cmd.Parameters.AddWithValue("@Ident_Earnings", Ident_Earnings);
                                        connection.Open();
                                        cmd.ExecuteNonQuery();
                                        connection.Close();
                                    }
                                }
                                else
                                {
                                    MessageBox.Show("Minimum earnings annaul value is 1");
                                }
                            }
                            else
                            {
                                MessageBox.Show("Minimum earnings monthly value is 1");
                            }
                        }
                        else
                        {
                            MessageBox.Show("Minimum age is 18, maximum 65");
                        }
                    }
                    else
                    {
                        MessageBox.Show("Enter Surname");
                    }
                }
                else
                {
                    MessageBox.Show("Enter Name");
                }
            }
            catch (SqlException sqlEx)
            {
                MessageBox.Show("Problem with connection with database");
            }
        }

        private void textBox3_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsDigit(e.KeyChar)&& e.KeyChar != (char)13 && e.KeyChar != (char)8 && e.KeyChar != (char)127 )
            {
                    e.Handled = true;
            }
            
        }

        private void textBox4_KeyPress(object sender, KeyPressEventArgs e)
        {

            if (!char.IsDigit(e.KeyChar) && e.KeyChar != (char)13 && e.KeyChar != (char)8 && e.KeyChar != (char)127)
            {
                e.Handled = true;
            }
        }

        private void textBox5_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsDigit(e.KeyChar) && e.KeyChar != (char)13 && e.KeyChar != (char)8 && e.KeyChar != (char)127)
            {
                e.Handled = true;
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            try
            {
                label7.Show();
                label6.Show();
                dataGridView1.Show();
                dataGridView2.Show();
                dataGridView2.DataSource = companyDataSet.Departments;
                this.departmentsTableAdapter.Fill(this.companyDataSet.Departments);
                using (SqlConnection connection = new SqlConnection("Server=localhost;Database=Company;Trusted_Connection=True;Connect Timeout=10;"))
                {
                    DataTable dataTable = new DataTable();

                    SqlCommand cmd = new SqlCommand(String.Format("Select Department,Departments.Ident_Department from Departments inner Join Ident on Departments.Ident_Department=Ident.Ident_Department Where Ident.Ident_Personality=@Ident_Personality;"));
                    cmd.CommandType = CommandType.Text;
                    cmd.Connection = connection;
                    cmd.Parameters.AddWithValue("@Ident_Personality", Ident_Personality);
                    connection.Open();
                    SqlDataAdapter da = new SqlDataAdapter(cmd);
                    da.Fill(dataTable);
                    connection.Close();
                    da.Dispose();
                    dataGridView1.DataSource = dataTable;
                }
            }
            catch (SqlException)
            {
                MessageBox.Show("Something is wrong with connection");
            }
        }

        private void dataGridView2_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            string second="as";
            string third = "pa";
            string fourth="ar";
            if(dataGridView1.Rows[0].Cells[0].Value.ToString()!=null)
            {
                second = dataGridView1.Rows[0].Cells[0].Value.ToString();
            }
            if (dataGridView1.Rows[1].Cells[0].Value != null)
            {
                third = dataGridView1.Rows[1].Cells[0].Value.ToString();
                if(dataGridView1.Rows[2].Cells[0].Value != null)
                {
                    fourth = dataGridView1.Rows[2].Cells[0].Value.ToString();
                }
            }

            
             
            if(fourth.Length<3)
            { 
            if (dataGridView1.Rows[0].Cells[0].Value.ToString() != dataGridView2.Rows[e.RowIndex].Cells[0].Value.ToString())
            {
                if (second != dataGridView2.Rows[e.RowIndex].Cells[0].Value.ToString())
                {
                    if (third != dataGridView2.Rows[e.RowIndex].Cells[0].Value.ToString())
                    {

                        Ident_Department = Convert.ToInt16(dataGridView2.Rows[e.RowIndex].Cells[1].Value.ToString());
                        try
                        { 
                        using (SqlConnection connection = new SqlConnection("Server=localhost;Database=Company;Trusted_Connection=True;Connect Timeout=10;"))
                        {
                            DataTable dataTable = new DataTable();
                            SqlCommand cmd = new SqlCommand(String.Format("Insert into Ident (Ident_Personality,Ident_Earnings,Ident_Department) values (@Ident_Personality,@Ident_Earnings,@Ident_Department);Select Department,Departments.Ident_Department from Departments inner Join Ident on Departments.Ident_Department=Ident.Ident_Department Where Ident.Ident_Personality=@Ident_Personality;"));
                            cmd.CommandType = CommandType.Text;
                            cmd.Connection = connection;
                            cmd.Parameters.AddWithValue("@Ident_Personality", Ident_Personality);
                            cmd.Parameters.AddWithValue("@Ident_Earnings", Ident_Earnings);
                            cmd.Parameters.AddWithValue("@Ident_Department", Ident_Department);
                            connection.Open();
                            SqlDataAdapter da = new SqlDataAdapter(cmd);
                            da.Fill(dataTable);
                            connection.Close();
                            da.Dispose();
                            dataGridView1.DataSource = dataTable;
                        }
                        }
                        catch (SqlException)
                        {
                            MessageBox.Show("Something is wrong with connection");
                        }
                   
                            }
                        else
                        {
                            MessageBox.Show("One person can be added to one department only once");
                        }
            }
            else
            {
                MessageBox.Show("One person can be added to one department only once");
            }
            }
            else
            {
                MessageBox.Show("One person can be added to one department only once");
            }
            }
            else
            {
                MessageBox.Show("One person can be maximum in 3 departments");
            }
           
        }

        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
            Ident_Department = Convert.ToInt16(dataGridView1.Rows[e.RowIndex].Cells[1].Value.ToString());
            using (SqlConnection connection = new SqlConnection("Server=localhost;Database=Company;Trusted_Connection=True;Connect Timeout=10;"))
            {
                    DataTable dataTable = new DataTable();
                    SqlCommand cmd = new SqlCommand(String.Format("Delete From Ident Where Ident_Personality=@Ident_Personality AND Ident_Earnings=@Ident_Earnings AND Ident_Department=@Ident_Department;Select Department,Departments.Ident_Department from Departments inner Join Ident on Departments.Ident_Department=Ident.Ident_Department Where Ident.Ident_Personality=@Ident_Personality;"));
                    cmd.CommandType = CommandType.Text;
                    cmd.Connection = connection;
                    cmd.Parameters.AddWithValue("@Ident_Personality", Ident_Personality);
                    cmd.Parameters.AddWithValue("@Ident_Earnings", Ident_Earnings);
                    cmd.Parameters.AddWithValue("@Ident_Department", Ident_Department);
                    connection.Open();
                    SqlDataAdapter da = new SqlDataAdapter(cmd);
                    da.Fill(dataTable);
                    connection.Close();
                    da.Dispose();
                    dataGridView1.DataSource = dataTable;
                }
            }
            catch(SqlException)
            {
                MessageBox.Show("Something is wrong with connection");
            }

        }

    }
}
